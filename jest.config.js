const config = {
    verbose: true,
    maxWorkers: 1,
    reporters: ['default'],
    testEnvironment: 'node',
    clearMocks: true,
    collectCoverage: true,
    coveragePathIgnorePatterns: [
        'node_modules',
        'coverage',
        'scripts',
        'assets',
    ],
    coverageThreshold: {
        global: {
            branches: 50,
            functions: 90,
            lines: 90,
            statements: 90,
        },
    }
};

module.exports = config;
