class User {
  constructor(name, email) {
    this.name = name;
    this.email = email;
  }

  logon() {
    return `${this.name} has logon successfully`;
  }

  logout() {
    return `${this.name} has logout successfully`;
  }
}

class Admin extends User {
  constructor(name, email, rwd) {
    super(name, email);
    this.rwd = rwd;
  }

  deleteUser() {
    if (this.rwd === true) {
      return 'delete allowed';
    } else {
      return 'delete not allowed';
    }
  }
}

module.exports.User = User;
module.exports.Admin = Admin;
