const fixture = require('fixture');

const sqs = () => ({
    getQueueUrl: (params) => ({
        promise: () => {
            if (params.QueueName === fixture.queueName) {
                return Promise.resolve({'QueueUrl': fixture.queueURL});
            } else {
                return Promise.reject(new Error('no queue found by name'));
            }
        }
    }),

    sendMessage: () => ({
        promise: () => Promise.resolve({'MessageId': fixture.uuid})
    }),

    deleteMessage: () => ({
        promise: () => Promise.resolve(fixture.deleteResponse)
    }),
});

const AWS = {
    SQS: jest.fn().mockImplementation(sqs)
};

module.exports = AWS;
