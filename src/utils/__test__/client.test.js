const {applyJob, calculateAllItems} = require('../client');

describe('give a client', () => {
  describe('when a client applies a job', () => {

    it('should send out an async message', async () => {
      const actual = await applyJob();
      expect(actual).toEqual('application received');
    });

  });
});

describe('give an array of items', () => {
  describe('a call back func supplied', () => {

    test('callback function invoked multiple times', () => {
      const items = ['test', 'test'];
      const calFunc = jest.fn(() => 4);
      calculateAllItems(items, calFunc);
      expect(calFunc.mock.calls.length).toBe(2);
      expect(calFunc.mock.calls[0][0]).toBe('test');
      expect(calFunc.mock.calls[1][0]).toBe('test');
    });

  });
});
