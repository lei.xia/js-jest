const uuid = '12345-abcdef-54321-fedcba';
const queueURL = 'http://localhost:4566/000000000000/notificationsDbApiBufferQueue';
const queueName = 'notificationsDbApiBufferQueue';
const queueARNPrefix = 'arn:aws:sqs:eu-west-2:000000000000:';
const queueARN = 'arn:aws:sqs:eu-west-2:000000000000:notificationsDbApiBufferQueue';
const deleteResponse = {
    ResponseMetadata: {RequestId: '12345-abcdef-54321-fedcba'}
};

module.exports = {
    queueURL,
    queueName,
    queueARN,
    deleteResponse,
    queueARNPrefix,
    uuid,
};
