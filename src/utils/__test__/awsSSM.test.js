const {getSsmByName} = require('../awsSSM');
const AWS = require('aws-sdk');

jest.mock('aws-sdk', () => {
    return {
        SSM: jest.fn()
    }
});

describe('ssm client', () => {
    describe('Get SSM by name', () => {
        beforeAll(() => {
            AWS.SSM.mockImplementationOnce(() => ({
                getParameter: jest.fn().mockReturnThis(),
                promise: jest.fn().mockReturnValueOnce(Promise.reject(new Error('No parameter configured.')))
                    .mockReturnValueOnce(Promise.resolve({
                        Parameter: {
                            Value: 'test-test'
                        }
                    }))
            }))
        });

        test('should throw an error', async () => {
            await expect(getSsmByName('Test-Param-Name')).rejects
                .toThrow(new Error("Failed GET SSM Test-Param-Name details: No parameter configured."))
        });

        test('should return parameter value', async () => {
            await expect(getSsmByName('Test-Param-Name')).resolves.toBe('test-test')
        })
    })
})
