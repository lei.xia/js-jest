const {lookUpSqsUrlByName, publish} = require('../awsSQS');

jest.mock('aws-sdk', () => {
    const client = () => ({

        sendMessage: (params) => ({
            promise: () => Promise.resolve({'MessageId': '123'})
        }),

        getQueueUrl: (params) => ({
            promise: () => {
                if (params.QueueName === 'test-q') {
                    return Promise.resolve({'QueueUrl': 'https://aws.com/test-queue'});
                } else {
                    return Promise.reject(new Error('queue not found'))
                }
            }
        }),
    });
    return {
        SQS: jest.fn().mockImplementation(client)
    }
});

describe('aws sqs utils', () => {

    describe('sqs queue look up', () => {

        test('should throw an error', async () => {
            await expect(lookUpSqsUrlByName('wrong-q-name')).rejects.toThrow(new Error('queue not found'));
        })

        test('should resolve queue name', async () => {
            await expect(lookUpSqsUrlByName('test-q')).resolves.toBe('https://aws.com/test-queue');
        })
    });

    describe('publish message to sqs queue', () => {
        // not working
        test('should publish message', async () => {
            const qUrl = "https//aws.com/test-queue";
            const attributes = {};
            const payload = {};
            await expect(publish(qUrl, attributes, payload)).resolves.toBe('123');
        })
    })
})
