const applyJob = async () => {
  await new Promise((resolve) => setTimeout(resolve, 3000));
  return 'application received';
};

const calculateAllItems = (items, calFunc) => {
  const result = [];
  items.forEach((item) => {
    result.push(calFunc(item));
  });
  return result;
};

module.exports = {
  applyJob,
  calculateAllItems,
};
