const AWS = require('aws-sdk');

let ssm = undefined;

const getSsmByName = async (name) => {
    if (!ssm) {
        ssm = new AWS.SSM({
            endpoint: "http://localhost:4566"
        });
    }
    return ssm.getParameter({ Name: name, WithDecryption: true })
        .promise()
        .then(result => result.Parameter.Value)
        .catch(err => {
            throw new Error(`Failed GET SSM ${name} details: ${err.message}`);
        });
}

module.exports = {getSsmByName};
