const AWS = require('aws-sdk');

let client = null;
let url_cache = {};

const sqsClientConfiguration = () => {
    if (!client) {
        client = new AWS.SQS({
            endpoint: 'http://localhost:4566'
        });
    }
}

const lookUpSqsUrlByName = async (qName) => {
    if (url_cache.hasOwnProperty(qName)) {
        return url_cache[`${qName}`];
    }
    sqsClientConfiguration();
    await client.getQueueUrl({'QueueName': qName} )
        .promise()
        .then(
        (data) => {
            url_cache[`${qName}`] = data.QueueUrl;
        }
    ).catch((err) => {
        throw err;
    })
    return url_cache[`${qName}`];
}

const publish = async (qUrl, attributes, payload) => {
    sqsClientConfiguration();
    const params = {
        MessageAttributes: attributes,
        MessageBody: payload,
        QueueUrl: qUrl
    };
    const response = await client.sendMessage(params).promise();
    return response.MessageId;
}

module.exports = {
    lookUpSqsUrlByName,
    publish
}
