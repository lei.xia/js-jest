const worker = require('./worker');

const instruct = (input) => {
  const arr = worker.process(input);
  arr.forEach((item) => {
    console.log(item);
  });
};

module.exports = instruct;
