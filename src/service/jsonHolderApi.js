const axios = require('axios');

const getResource = async (id) => {
    const host = `https://jsonplaceholder.typicode.com/todos/${id}`;
    try {
        const result = await axios.get(host);
        return result.data;
    } catch (error) {
        console.log(`failed to get resource from ${host} with error ${error.message}`);
    }
}
module.exports = {getResource}
