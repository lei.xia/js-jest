const worker = require('../worker');

describe('worker', () => {
  it('should split input into an array', () => {
    const actual = worker.process('1 2 3');
    console.log(actual);
    expect(actual).toEqual(['1', '2', '3']);
  });
});
