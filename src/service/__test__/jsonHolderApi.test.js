const jsonHolderApi = require('../jsonHolderApi');
const axios = require('axios');

jest.mock('axios', () => {
    return {
        get: jest.fn().mockImplementation(
            () => Promise.resolve(
                {
                         data: { title: 'test-test' }
                       }
                 ))
    }
});

describe("json holder api", () => {
    let expected = undefined;
    beforeEach(() => {
        expected = {
            title: 'test-test'
        };
    })
    it('should return data object', async () => {
        const actual = await jsonHolderApi.getResource(1);
        expect(actual).toEqual(expected);
        expect(axios.get).toBeCalledTimes(1);
        expect(axios.get).toBeCalledWith(`https://jsonplaceholder.typicode.com/todos/1`)
    });
})
