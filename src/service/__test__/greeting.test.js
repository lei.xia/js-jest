const {Greeting} = require('../greeting');

describe('greeting service', () => {
  describe('given a name', () => {

    it('when greeting called returns hello message', () => {
      const greeter = new Greeting('Lei Xia');
      expect(greeter.greet()).toEqual('Hello Lei Xia');
    });

  });
});
