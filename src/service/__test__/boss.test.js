const worker = require('../worker');
const instruct = require('../boss');

describe('boss', () => {
  worker.process = jest.fn((input) => ['a', 'b', 'c']);

  it('worker should be called once', () => {
    instruct('SOMETHING');
    expect(worker.process).toHaveBeenCalledWith('SOMETHING');
  });

});
