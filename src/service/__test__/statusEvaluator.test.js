const evalClaim = require('../statusEvaluator');

describe('Claim evaluator', () => {
  describe('A claim', () => {
    const positiveClaim = {
      STATUS: 'PROCESSED',
    };

    test('should return true', () => {
      expect(evalClaim(positiveClaim)).toBeTruthy();
    });

    const negativeClaim = {
      STATUS: 'REJECTED',
    };

    test('should return false', () => {
      expect(evalClaim(negativeClaim)).toBeFalsy();
    });

    const outClaim = {
      STATUS: 'UNKNOWN',
    };

    test('should throw error with message', () => {
      expect(() => {
        evalClaim(outClaim);
      }).toThrow('UNKNOWN status can not be determined');
    });
  });
});

