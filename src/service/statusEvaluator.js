const APPLICATION_STATUS = require('../enums/applicationStates');

const evalClaim = (claim) => {
  switch (claim.STATUS) {
    case APPLICATION_STATUS.PROCESSED:
    case APPLICATION_STATUS.STARTED:
    case APPLICATION_STATUS.SUBMITTED:
      return true;
    case APPLICATION_STATUS.REJECTED:
      return false;
    default:
      throw new Error(`${claim.STATUS} status can not be determined`);
  }
};
module.exports = evalClaim;
