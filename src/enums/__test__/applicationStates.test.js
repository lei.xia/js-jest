const APPLICATION_STATUS = require('../applicationStates');

describe('Application status', () => {
  it('should contain STARTED', () => {
    expect(APPLICATION_STATUS.STARTED).toEqual('STARTED');
  });

  it('should contain SUBMITTED', () => {
    expect(APPLICATION_STATUS.SUBMITTED).toEqual('SUBMITTED');
  });

  it('should contain PROCESSED', () => {
    expect(APPLICATION_STATUS.PROCESSED).toEqual('PROCESSED');
  });

  it('should contain REJECTED', () => {
    expect(APPLICATION_STATUS.REJECTED).toEqual('REJECTED');
  });
});
