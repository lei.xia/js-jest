function movie(title, rating, publisher) {
  this.title = title;
  this.rating = rating;
  this.publisher = publisher;
}

module.exports = {Movie: movie};
