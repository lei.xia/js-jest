const {Movie} = require('../movie');

describe('movie', () => {
  it('has following properties', () => {
    const goodWillHunting = new Movie('good will hunting', '7.0', 'https://www.sony.com');
    expect(goodWillHunting.title).toEqual('good will hunting');
    expect(goodWillHunting.rating).toEqual('7.0');
    expect(goodWillHunting.publisher).toEqual('https://www.sony.com');
  });
});
