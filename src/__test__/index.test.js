const {User, Admin} = require('../index');

describe('given a user', () => {
  describe('with name and email', () => {
    const leixia = new User('LeiXia', 'leixia@test.com');
    test('login successfully', () => {
      expect(leixia.logon()).toEqual('LeiXia has logon successfully');
    });

    test('logout successfully', () => {
      expect(leixia.logout()).toEqual('LeiXia has logout successfully');
    });
  });

  describe('with admin role', () => {
    describe('with an admin user can delete', () => {
      const adminUser = new Admin('JohnSmith', 'john.smith@test.com', false);
      test('login successfully', () => {
        expect(adminUser.logon()).toEqual('JohnSmith has logon successfully');
      });

      test('delete operation not allowed', () => {
        expect(adminUser.deleteUser()).toEqual('delete not allowed');
      });
    });

    describe('with an admin user can not delete', () => {
      const adminUser = new Admin('JohnSmith', 'john.smith@test.com', true);
      test('login successfully', () => {
        expect(adminUser.logon()).toEqual('JohnSmith has logon successfully');
      });
      test('delete operation not allowed', () => {
        expect(adminUser.deleteUser()).toEqual('delete allowed');
      });
    });
  });
});
